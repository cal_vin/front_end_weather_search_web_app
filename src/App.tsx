import React from 'react';
import logo from './logo.svg';
import './App.css';
import BackendExtService from './extService/BackendExtService';
import { Col, Row, Table } from 'react-bootstrap';
import ReactBootstrap from 'react-bootstrap';
import { WeatherDataDto } from './domain/backendDto';

type Props = {}; // save things from superclass
type State = {
  cityname?: string,
  currTemp?: string,
  currTempMin?: number,
  currTempMax?: number,
  currentHumidity?: string,
  currentWeatherDescr?: string
}; // save things from its own Ui

export default class App extends React.Component<Props, State> {
  state = {} as State;

  constructor(props: Props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.submit = this.submit.bind(this);
    this.renderWeatherData = this.renderWeatherData.bind(this);
  }

  handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  submit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    BackendExtService.getWeatherData(this.renderWeatherData);
  }

  renderWeatherData(data: WeatherDataDto) {
    console.log(data.base);
    console.log(data);

    this.setState({currTemp: data.base})
  }

  render() {
    return (
      <div className="App">

        <header className="App-header">

          <img src={logo} className="App-logo" alt="logo" />
          <p>Weather Searching</p>

        </header>

        <body className="App-body">
          
          <form onSubmit={this.submit}>
            <h3>City Name</h3>
            <input type="text" name='cityname' onChange={this.handleInputChange}/>

            <input type="submit" value="SUBMIT"/>
          </form>
        
          <br/>
          <div className="App-response">

            <table className="table table-bordered table-dark">
              <thead>
                <tr>
                  <th>1</th>
                  <th>2</th>
                  <th>3</th>
                  <th>4</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>{this.state.cityname}</td>
                  <td>{this.state.currTemp}</td>
                  <td>@mdo</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                </tr>
              
              </tbody>
            </table>
 
          </div>

        </body>

      </div>
    );
  }
}
