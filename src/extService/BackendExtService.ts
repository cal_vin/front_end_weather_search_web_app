import axios, { AxiosResponse } from "axios";
import { WeatherDataDto } from "../domain/backendDto";

export default class BackendExtService {
    static getWeatherData(callback: (data: WeatherDataDto) => void): void {
        axios.get("https://api.openweathermap.org/data/2.5/weather?lat=44.3472638&lon=10.990407&units=metric&appid=9c8f5a888d7badacd37984276f2168d2")
        .then((response: AxiosResponse<WeatherDataDto>) => {
            if (response.status === 200) {
                
                callback(response.data as WeatherDataDto);
                
            } else {
                console.log(response)
            }
        }).catch(function(error) {
            console.log(error);
        });
    }
}